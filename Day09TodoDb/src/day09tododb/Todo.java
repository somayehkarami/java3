package day09tododb;

import java.util.Date;
import java.text.SimpleDateFormat;

class Todo {

    public Todo(int id, String task, int difficulty, Date dueDate, Status status) {
        this.id = id;
        this.task = task;
        this.difficulty = difficulty;
        this.dueDate = dueDate;
        this.status = status;
    }

    int id;
    String task; // 1-100 characters, made up of uppercase and lowercase letters, digits, space, _-*?%#@(),./\ only
    int difficulty; // 1-5, as slider
    Date dueDate; // year 1900-2100 both inclusive, use Formatted Field (in Palette), not Text Field.
    Status status; // in database enumeration/ENUM, in GUI combo box

    enum Status {
        Pending, Done, Delegated
    }
static SimpleDateFormat dateFormatDisplay = new SimpleDateFormat("yyyy-MM-dd");
    
    @Override
    public String toString() {
        return "Todo{" + "id=" + id + ", task=" + task + ", difficulty=" + difficulty + ", dueDate=" + dueDate + ", status=" + status + '}';
        
    }

}
