package day09tododb;

import com.mysql.cj.xdevapi.Statement;
import day09tododb.Todo.Status;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Database {

    public static final SimpleDateFormat dateFormatSQL = new SimpleDateFormat("yyyy-MM-dd");
    //call connection
    private static final String dbURL = "jdbc:mysql://localhost:3306/day09Todo";//(day08people)name of schema
    private static final String username = "root";
    private static final String password = "Elsasorin";

    private Connection conn;// no access to database outside of this class

    // open database connection
    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbURL, username, password);
    }

    // select 
    public ArrayList<Todo> getAllTodo() throws SQLException {  //Data Layer
        try {
            ArrayList<Todo> list = new ArrayList<>();
            // the purpose is to create a new person
            String sql = "SELECT * FROM todo";// String sql = "SELECT * FROM people WHERE id =?";
            PreparedStatement statement = conn.prepareStatement(sql);
            // it is a good practice to use try-with-resources for ResultSet so it is freed up as soon as possible
            try (ResultSet result = statement.executeQuery(sql)) {
                while (result.next()) { // has next row to read
                    int id = result.getInt("id");
                    String task = result.getString("Task");
                    int difficulty = result.getInt("difficulty");
                    Date dueDate = result.getDate("dueDate");//getDate
                    String statusStr = result.getString("status");
                    Todo.Status status = Todo.Status.valueOf(statusStr);
                    Todo todo = new Todo(id, task, difficulty, dueDate, status);//inictiation the todo
                    list.add(todo);

                }
                return list;// return the array list
            }
        } catch (IllegalArgumentException ex) {
            throw new SQLException("Error creating todo from record", ex); // exception chaining
        }
    }

    public void addTodo(Todo todo) throws SQLException {

        String sql = "INSERT INTO people VALUES  (NULL, ?, ?,?,?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        PreparedStatement stmtInsert;

        //PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, todo.task);//for name
        statement.setInt(2, todo.difficulty);//for age"" turn age to String
        statement.setDate(3, new java.sql.Date(todo.dueDate.getTime()));
        statement.setString(4, todo.status + "");//comboStatus.setSelectedItem

        //statement.setStatus(4, todo.status);
        statement.executeUpdate(); // for insert, update, delete
        System.out.println("Record inserted");
    }

    public void updateTodo(Todo todo) throws SQLException {
        String sql = "UPDATE INTO people SET task=?, difficulty=?  WHERE id =?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, todo.task);//for task
        statement.setInt(2, todo.difficulty);//for difficulty
        statement.setDate(3, new java.sql.Date(todo.dueDate.getTime()));
        statement.setString(4, todo.status + "");//comboStatus.setSelectedItem
        statement.setInt(5, todo.id);//for ID
        statement.executeUpdate(); // for insert, update, delete
        System.out.println("Record inserted id=" + todo.id);//Never update ID
    }

}
