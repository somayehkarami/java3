
package day10students;


public class Student {

    public Student(int id, String name, byte[] image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }
    
    
    int id;
    String name;
    byte[] image;

    @Override
    public String toString() {
        //return "Student{" + "id=" + id + ", name=" + name + '}';
        return String.format("%d: %s",id,name);
    }
    
    
    
    
    
}
