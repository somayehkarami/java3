package day10students;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Database {

    private static final String dbURL = "jdbc:mysql://localhost:3306/day10student";//(day08people)name of schema
    private static final String username = "root";
    private static final String password = "Elsasorin";

    private Connection conn;// no access to database outside of this class

    // open database connection
    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbURL, username, password);
    }
// select 

    public ArrayList<Student> getAllStudents() throws SQLException {//data Layer
        ArrayList<Student> list = new ArrayList<>();

        String sql = "SELECT id,name FROM students";// String sql = "SELECT * FROM people WHERE id =?";
        PreparedStatement statement = conn.prepareStatement(sql);
        // it is a good practice to use try-with-resources for ResultSet so it is freed up as soon as possible
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) { // has next row to read
                int id = result.getInt("id");
                String name = result.getString("name");
                // we dont ftch the photo

                list.add(new Student(id, name, null));

            }
        }
        return list;
    }
//fetch one record, including the BLOB

    Student getStudentById(int id) throws SQLException {
        PreparedStatement StmtSelect = conn.prepareStatement("SELECT * FROM students WHERE id=?");
        StmtSelect.setInt(1, id);
        try (ResultSet resultset = StmtSelect.executeQuery()) {
            if (resultset.next()) {
                String name = resultset.getString("name");
                byte[] image = resultset.getBytes("image");
                return new Student(id, name, image);
            } else {

                throw new SQLException("Record notfound");
            }

        }
    }
//insert

    public void addStudent(Student student) throws SQLException {
        String sql = "INSERT INTO students VALUES (NULL, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, student.name);//for name
        statement.setBytes(2, student.image);
        statement.executeUpdate(); // for insert, update, delete
        System.out.println("Record inserted");
    }

    //=================================
    public void updateStudent(Student student) throws SQLException {
        String sql = "UPDATE INTO people SET name=?, age=?  WHERE id =?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, student.name);//for name
        statement.setBytes(2, student.image);//for age"" turn age to String
        statement.setInt(3, student.id);
        statement.executeUpdate(); // for insert, update, delete
        System.out.println("Record inserted id=" + student.id);//Never update ID
    }

    public void deleteStudent(int id) throws SQLException {
        String sql = "DELETE FROM students WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        statement.executeUpdate();
        System.out.println("Record students id=" + id);
    }

    //TODO: implement update and delete
}
