package day02teammembers;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Day02TeamMembers {

    HashMap<String, ArrayList<String>> playersByTeams = new HashMap<>();
    static ArrayList<String> namesList = new ArrayList<>();
    static ArrayList<String> teamList = new ArrayList<>();
    static final String DATA_FILE_NAME = "team.txt";

    static void readDataFromFile() {
        try (Scanner fileInput = new Scanner(new File(DATA_FILE_NAME))) {
            while (fileInput.hasNextLine()) {
                String line = fileInput.nextLine();
                String[] res = line.split(":");
                teamList.add(res[0]);
                namesList.add(res[1]);
                
            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
        System.out.println(namesList);
        System.out.println(teamList);
        //HashSet<String> hset = new HashSet<String>(namesList ); 
        // Set uniqueValues = new HashSet<>(namesList);
        //System.out.println("ArrayList Unique Values is : "+ uniqueValues);

    }

    public static void main(String[] args) {
        readDataFromFile();
    }

}
//Key: name of the player
//Value: list of teams player belongs to
/*Example output:
Jerry plays in: Green Grass Team, Terrific Team Today, Other Best Team, Yet Another Super-Team
Marry plays in: Green Grass Team, Terrific Team Today, Yet Another Super-Team
Barry plays in: ... (and so on)*/
