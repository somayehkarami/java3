package day02people;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Day02People {

    static ArrayList<Person> people = new ArrayList<>();
    static final String DATA_FILE_NAME = "people.txt";

    private static void readDataFromFile() {
        int ctr = 0;
        try (Scanner fileInput = new Scanner(new File(DATA_FILE_NAME))) {
            while (fileInput.hasNextLine()) {
                //ctr = ctr + 1; to count the Lines
                String line = fileInput.nextLine();
                try {
                    String data[] = line.split(";");
                    if (data.length != 2) {
                        //System.out.println("Invalid number of fields in line, skipping.");
                        //continue;
                        throw new InvalidDataException("Every line must have 2 fields");

                    }
                    String name = data[0];
                    int age = Integer.parseInt(data[1]);// ex NumberFormatException
                    Person person = new Person(name, age);
                    people.add(person);
                } catch (NumberFormatException ex) {
                    System.out.println("Error parsing integer, skipping the invalid line");
                    System.out.println(">> " + line);
                } catch (InvalidDataException ex) {// this catch is for seterr
                    System.out.println("Error in data, skippnig line: " + ex.getMessage());
                    System.out.println(">> " + line);
                }

            }
        } catch (IOException ex) {
            System.out.println("Error reading file: " + ex.getMessage());
        }
    }

    private static void printDataToScreen() {
        for (Person p : people) {
            System.out.println(p); // toString();
        }
    }

    public static void main(String[] args) {
     // Person p1= new Person();
     //Person p2 = new Person("Jenet",23);
     //p1.setName("Jerry boe hjk dfg");
     
        readDataFromFile();
        printDataToScreen();

    }

}
















/*
    public static void main(String[] args) {        
        try {
            File f = new File("people.ser");
            Scanner sc = new Scanner(f);

            List<Person> people = new ArrayList<Person>();

            while(sc.hasNextLine()){
                String line = sc.nextLine();
                String[] details = line.split(":");
                String gender = details[0];
                String name = details[1];
                int age = Integer.parseInt(details[2]);
                Person p = new Person(gender, name, age);
                people.add(p);
            }

            for(Person p: people){
                System.out.println(p.toString());
            }

        } catch (FileNotFoundException e) {         
            e.printStackTrace();
        }
    }
}*/
