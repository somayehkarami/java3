package day02people;

import java.security.InvalidParameterException;

public class Person {

    private String name;
    private int age;

    public Person(String name, int age) throws InvalidDataException {
        //this.name = name;
        //this.age = age;

        setName(name);
        setAge(age);

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) throws InvalidDataException {// 1-150
        if (age < 1 || age > 150) {
            throw new InvalidDataException(" Age must be between 1-150");
        }
        this.age = age;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws InvalidDataException {
        if (name.length() < 2 || name.length() > 20 || name.contains(";")) {
            throw new InvalidDataException(" Name must be 2 to 20 characters long and not contain semicolons");
        }
        this.name = name;
    }

    @Override
    public String toString() {
        //return "Person{" + "name=" + name + ", age=" + age + '}';
        return String.format("%s is %d y/o", name, age);
    }

}
