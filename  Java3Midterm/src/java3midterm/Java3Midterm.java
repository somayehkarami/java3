package java3midterm;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Java3Midterm {

    static Scanner input = new Scanner(System.in);
    static ArrayList<IceCreamOrder> ordersList = new ArrayList<>();

    static int inputInt() {
        while (true) {
            try {
                int result = input.nextInt(); // ex InputMismatchException
                input.nextLine(); // consume the leftover newline
                return result;
            } catch (InputMismatchException ex) {
                System.out.println("Invalid input, enter an integer. Try again.");
                input.nextLine(); // consume the invalid input
            }
        }
    }

    static int getMenuChoice() {
        System.out.print("Available options[0-3]:\n"
                + "1. Add an order\n"
                + "2. List orders by customer name\n"
                + "3. List orders by delivery date\n"
                + "0. Exit\n"
                + "Please enter your choice: ");
        int choice = inputInt();
        return choice;
    }
    final static String DATA_FILE_NAME = "orders.txt";

    public static void main(String[] args) {
        readDataFromFile();
        while (true) {
            //System.out.println("Current instance count: " + Todo.getInstanceCount());
            int choice = getMenuChoice();
            switch (choice) {
                case 1:
                    addOrder();
                    break;
                case 2:
                    listOrdersByCustomer();
                    break;
                case 3:
                    listOrdersByDelivery();
                    break;

                case 0:
                    saveDataToFile();
                    System.out.println("Exiting.");
                    return;
                default:
                    System.out.println("Invalid choice, try again");
            }
            System.out.println();
        }
    }

    private static void addOrder() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        try {
            System.out.println("Adding an order.");
            System.out.print("Please enter customer's name: ");
            String customerName = input.nextLine();
            System.out.print("Enter the delivery date (yyyy-mm-dd): ");
            String dueDateStr = input.nextLine();
            Date deliveryDate = Globals.dateFormatScreen.parse(dueDateStr); // ex ParseException
            System.out.print("Enter delivery time (hh:mm):");
            String timeStr = input.nextLine();
            SimpleDateFormat format = new SimpleDateFormat(timeStr);
            System.out.print("Enter hours of work (integer): ");
            int hours = input.nextInt(); // ex NumberFormatException
            System.out.println("Available flavours: VANILLA, CHOCOLATE, STRAWBERRY, ROCKYROAD   ");
            System.out.println("Enter which flavour to add, empty to finish:");
            //for(String s : flaver){
            // flavList.add(Flavour.valueOf(s.toUpperCase().trim()));  // ex IllegalArgumentException, NullPointerException
            // }
            IceCreamOrder todo = newIceCreamOrder(customerName, deliveryDate, flavList); // ex DataInvalidException
            ordersList.add(todo);
            System.out.println("Order accepted:");
            System.out.println(todo);
        } catch (ParseException | NumberFormatException ex) {
            System.out.println("Error parsing: " + ex.getMessage());
        } catch (DataInvalidException ex) {
            System.out.println("Error: " + ex.getMessage());
        }
    }

    private static void listOrdersByCustomer() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (ordersList.isEmpty()) {
            System.out.println("No todos found");
            return;
        }

        for (int i = 0; i < ordersList.size(); i++) {
            ordersList.sort((p1, p2) -> p1.customerName.compareTo(p2.customerName));
            System.out.printf("#%d: %s\n", i + 1, ordersList.get(i));
        }
        //Collections.sort ordersList, (p1, p2) -> p1.customerName.compareTo(p2.customerName));

    }

    private static void listOrdersByDelivery() {
        for (int i = 0; i < ordersList.size(); i++) {
            ordersList.sort((p1, p2) -> p1.deliveryDate.compareTo(p2.deliveryDate));
            System.out.printf("#%d: %s\n", i + 1, ordersList.get(i));
        }

    }

    private static void readDataFromFile() {
        try (Scanner inputFile = new Scanner(new File(DATA_FILE_NAME))) {
            while (inputFile.hasNextLine()) {
                String line = "";
                try {
                    line = inputFile.nextLine();
                    ordersList.add(new IceCreamOrder(line)); // ex DataInvalidException
                } catch (DataInvalidException ex) {
                    System.out.printf("Error parsing line (%s) ignoring: %s\n", ex.getMessage(), line);
                }
            }
        } catch (FileNotFoundException ex) {
            // ignore, it's okay if the file is not there
        }
    }

    private static void saveDataToFile() {
        try (PrintWriter outputFile = new PrintWriter(new File(DATA_FILE_NAME))) {
            for (IceCreamOrder todo : ordersList) {
                outputFile.println(todo.toDataString());
            }
        } catch (IOException ex) {
            System.out.println("Error writing data back to file");
        }
    }
}
