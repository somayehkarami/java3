package java3midterm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class IceCreamOrder {

    public IceCreamOrder(String customerName, Date deliveryDate, ArrayList<Flavour> flavList) throws DataInvalidException {
        setCustomerName(customerName);
        setDeliveryDate(deliveryDate);
        setFlavList(flavList);

    }

    public IceCreamOrder(String dataLine) throws DataInvalidException { // throws ParseException, NumberFormatException, IllegalArgumentException {
        String[] data = dataLine.split(";");
        if (data.length != 3) {
            throw new DataInvalidException("Invalid number of items in line");
        }
        try {
            // continue parsing, use setters to set values
            String customerName = data[0];
            Date deliveryDate = dateFormatFile.parse(data[1]); // ex ParseException
            String[] flavList = data[3].split(","); // ex DataInvalidException
            for (String s : flavers) {
                flavList.add(Flavour.valueOf(s.toUpperCase().trim()));
            }// ex IllegalArgumentException, NullPointerException
            setCustomerName(customerName); // ex DataInvalidException
            setDeliveryDate(deliveryDate); // ex DataInvalidException
            setFlavList(flavList);
        } catch (ParseException ex) {
            throw new DataInvalidException("Date format invalid", ex);
        } catch (NumberFormatException ex) {
            throw new DataInvalidException("Integer value expected", ex);
        }
    }

    String customerName; // 2-20 characters, regex to allow only uppercase, lowercase, digits, space, -,.()'"
    Date deliveryDate; // 'y-m-d h:m' must be right now or later, and no further than 100 days ahead - get current date/time from new Date() and use Calendar
    private ArrayList<Flavour> flavList = new ArrayList<Flavour>();// duplicates allowed, but setter must verify that list must not be empty (at least 1 item in it)

    enum Flavour {
        VANILLA, CHOCOLATE, STRAWBERRY, ROCKYROAD
    };

    @Override
    public String toString() {

        return String.format(" %s;%s;%s", customerName, deliveryDate, flavList);
    }

    private static final SimpleDateFormat dateFormatFile;

    static { // static initializer
        dateFormatFile = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatFile.setLenient(false);
    }

    public String toDataString() {
        // returns a string similar to "Buy milk 2%;2019-11-22;2"
        String dueDateStr = dateFormatFile.format(deliveryDate);
        return String.format(" %s;%s;%s", customerName, deliveryDate, flavList);
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) throws DataInvalidException {
        if (!customerName.matches("[a-zA-Z0-9()'\". -]{2,20}")) {
            throw new DataInvalidException("Task must be 2-20 characters,   you allow to use uppercase, lowercase, digits, space, -,.()'\"");
        }
        this.customerName = customerName;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) throws DataInvalidException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(deliveryDate);
        Date d = new Date();
        if (deliveryDate.before(d) || deliveryDate.equals(d)) {

            throw new DataInvalidException("'y-m-d h:m' must be right now or later,");
        }
        this.deliveryDate = deliveryDate;
    }

    public ArrayList<Flavour> getFlavList() {
        return flavList;
    }

    public void setFlavList(ArrayList<Flavour> flavList) throws DataInvalidException {
        if (flavList.isEmpty()) {
            throw new DataInvalidException("list must not be empty ");
        }
        this.flavList = flavList;
    }

}
