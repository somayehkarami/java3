package java3midterm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

public class Java3MidtermTest {

    @Test
    public void testToDataString() throws ParseException, DataInvalidException {
        System.out.println("toDataString");
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("20-10-2001"); // ex
        IceCreamOrder instance = new IceCreamOrder("Run this test", customerName, date, flavList); // ex
        String expResult = "Run this test;Alice Joy ;2001-10-20;flavListPending";
        String result = instance.toDataString();
        assertEquals(expResult, result);
    }

    @Test
    public void testDataLineConstructor() throws DataInvalidException, ParseException {
        System.out.println("Todo(String dataLine)");
        IceCreamOrder instance = new IceCreamOrder("Run this test;Alice Joy;2001-10-20;3;Done"); // ex
        assertEquals("Run this test", instance.getCustomerName());
        Date date = new SimpleDateFormat("dd/MM/yyyy").parse("20/10/2001"); // ex
        assertEquals(date, instance.getDeliveryDate());
        assertEquals(instance.getFlavList());
    }

    @Test(expected = DataInvalidException.class)
    public void testDueDateInvalid() throws DataInvalidException, ParseException {
        System.out.println("Todo.setDeliveryDate(invalid)");
        Date invalidDate = new SimpleDateFormat("dd/MM/yyyy").parse("20/10/2101"); // ex
        IceCreamOrder instance = new IceCreamOrder("Test"
        Alice Joy, invalidDate, flavours ROCKYROAD, ROCKYROAD, CHOCOLATE
    

    , ); // must throw exception
    }


    @Test
    public void testInputInt() {
        System.out.println("inputInt");
        int expResult = 0;
        int result = Java3Midterm.inputInt();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getMenuChoice method, of class Java3Midterm.
     */
    @Test
    public void testGetMenuChoice() {
        System.out.println("getMenuChoice");
        int expResult = 0;
        int result = Java3Midterm.getMenuChoice();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of main method, of class Java3Midterm.
     */
    @Test
    public void testMain() {
        System.out.println("main");
        String[] args = null;
        Java3Midterm.main(args);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

}
