package day09peopledb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import static java.util.Collections.list;
import java.sql.*;

public class Database {
    //call connection

    private static final String dbURL = "jdbc:mysql://localhost:3306/day08people";//(day08people)name of schema
    private static final String username = "root";
    private static final String password = "Elsasorin";

    private Connection conn;// no access to database outside of this class

    // open database connection
    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbURL, username, password);
    }
// select 
    public ArrayList<Person> getAllPepole() throws SQLException {//data Layer
        ArrayList<Person> list = new ArrayList<>();

        String sql = "SELECT * FROM people";// String sql = "SELECT * FROM people WHERE id =?";
        PreparedStatement statement = conn.prepareStatement(sql);
        // it is a good practice to use try-with-resources for ResultSet so it is freed up as soon as possible
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) { // has next row to read
                int id = result.getInt("id");
                String name = result.getString("name");
                int age = result.getInt("age");
                list.add(new Person(id, name, age));

            }
        }
        return list;
    }
//insert
    public void addPerson(Person person) throws SQLException {
        String sql = "INSERT INTO people VALUES (NULL, ?, ?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, person.name);//for name
        statement.setInt(2, person.age);//for age"" turn age to String
        statement.executeUpdate(); // for insert, update, delete
        System.out.println("Record inserted");
    }
    public void updatePerson(Person person) throws SQLException{
        String sql = "UPDATE INTO people SET name=?, age=?  WHERE id =?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, person.name);//for name
        statement.setInt(2, person.age);//for age"" turn age to String
        statement.setInt(2, person.id);
        statement.executeUpdate(); // for insert, update, delete
        System.out.println("Record inserted id=" +person.id);//Never update ID
    }
    public void deletePerson(int id) throws SQLException {
        String sql = "DELETE FROM people WHERE id=?";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        statement.executeUpdate();
        System.out.println("Record deleted id=" + id);
    }
}
