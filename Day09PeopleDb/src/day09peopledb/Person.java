
package day09peopledb;

public class Person {

    public Person(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
    
    int id;
    String name;
    int age;

    @Override
    public String toString() {
        return String.format("%d: %s is %d y/o", id, name, age);
    }
    
}

