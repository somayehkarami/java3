package day04appointments;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Day04Appointments {

    static Scanner input = new Scanner(System.in);
    static ArrayList<Appointment> todoList = new ArrayList<>();

    static int inputInt() {
        while (true) {
            try {
                int result = input.nextInt(); // ex InputMismatchException
                input.nextLine(); // consume the leftover newline
                return result;
            } catch (InputMismatchException ex) {
                System.out.println("Invalid input, enter an integer. Try again.");
                input.nextLine(); // consume the invalid input
            }
        }
    }

    static int getMenuChoice() {
        System.out.print("Please make a choice [0-4]:\n"
                + "1. Make an appointment\n"
                + "2. List appointments by Date\n"
                + "3. List appointments by Name\n"
                + "4. List appointments by their first Reason\n"
                + "0. Exit\n"
                + "Your choice is: ");
        int choice = inputInt();
        return choice;
    }
    final static String DATA_FILE_NAME = "appointments.txt";

    public static void main(String[] args) {
        readDataFromFile();
        while (true) {
            //System.out.println("Current instance count: " + Todo.getInstanceCount());
            int choice = getMenuChoice();
            switch (choice) {
                case 1:
                    makeAppointment();
                    break;
                case 2:
                    dateAppointment();
                    break;
                case 3:
                    nameAppointment();
                    break;
                case 4:
                    reasonAppointment();
                    break;
                case 0:
                    saveDataToFile();
                    System.out.println("Exiting.");
                    return;
                default:
                    System.out.println("Invalid choice, try again");
            }
            System.out.println();
        }
    }

    private static void makeAppointment() {
        try {
            System.out.print("Enter appointment date (yyyy-mm-dd):");
            String dueDateStr = input.nextLine();
            Date date = Globals.dateFormatScreen.parse(dueDateStr); // ex ParseException
            System.out.print("Enter appointment time (hh:mm):");
            String timeStr = input.nextLine();
            SimpleDateFormat format = new SimpleDateFormat(timeStr);
            System.out.print("Enter duration (20, 40, or 60):");
            int durationMinutes = input.nextInt(); // ex NumberFormatException
            System.out.print("Enter your name:");
            String name = input.nextLine();
            System.out.print("Enter comma-separate list of reasons for visit:");
            String reasonList = input.nextLine();
            Appointment list = new Appointment(date, durationMinutes, name, Reason.CHECKUP); // ex DataInvalidException
            todoList.add(list);
            System.out.print("Appointment created.");

        } catch (ParseException | NumberFormatException ex) {
            System.out.println("Error parsing: " + ex.getMessage());
        }
    }

    private static void dateAppointment() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void nameAppointment() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void reasonAppointment() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private static void saveDataToFile() {
        try (PrintWriter outputFile = new PrintWriter(new File(DATA_FILE_NAME))) {
            for (Appointment todo : todoList) {
                outputFile.println(todo.toDataString());
            }
        } catch (IOException ex) {
            System.out.println("Error writing data back to file");
        }

    }

    private static void readDataFromFile() {

        try (Scanner inputFile = new Scanner(new File(DATA_FILE_NAME))) {
            while (inputFile.hasNextLine()) {
                String line = "";
                try {
                    line = inputFile.nextLine();
                    todoList.add(new Appointment(line)); // ex DataInvalidException
                } catch (DataInvalidException ex) {
                    System.out.printf("Error parsing line (%s) ignoring: %s\n", ex.getMessage(), line);
                }
            }
        } catch (FileNotFoundException ex) {
            // ignore, it's okay if the file is not there
        }
    }
}
