package day04appointments;

import java.awt.desktop.UserSessionEvent.Reason;
import static java.lang.String.valueOf;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

public class Appointment {

    public Appointment(Date date, int durationMinutes, String name, HashSet<Reason> reasonList) throws DataInvalidException {
        setDate(date);
        setDurationMinutes(durationMinutes);
        setName(name);
        setReasonList(reasonList);

    }
    static final SimpleDateFormat dateFormatScreen = new SimpleDateFormat("yyyy/MM/dd");

    public Appointment(String dataLine) throws DataInvalidException { // throws ParseException, NumberFormatException, IllegalArgumentException {
        String[] data = dataLine.split(";");
        if (data.length != 4) {
            throw new DataInvalidException("Invalid number of items in line");
        }
        try {
            // continue parsing, use setters to set values

            Date date = dateFormatFile.parse(data[0]); // ex ParseException
            int durationMinutes = Integer.parseInt(data[1]); // ex NumberFormatException
            String name = data[2];// ex IllegalArgumentException
            String[] reasons = data[3].split(","); // ex DataInvalidException
            for (String s : reasons) {
                reasonList.add(Reason.valueOf(s.toUpperCase().trim())); 
               // ex IllegalArgumentException, NullPointerException
            }
            setDate(date); // ex DataInvalidException
            setDurationMinutes(durationMinutes); // ex DataInvalidException
            setName(name); //ex DataInvalidException
            setReasonList(reasonList);
            //

        } catch (ParseException ex) {
            throw new DataInvalidException("Date format invalid", ex);
        } catch (NumberFormatException ex) {
            throw new DataInvalidException("Integer value expected", ex);
        }
    }

    private Date date; // year, month, day, hours and minutes, appointment can only be made for tomorrow or a later date
    private int durationMinutes; // setters verify only 20,40,60 is allowed,
    private String name; // regex to allow only uppercase, lowercase, digits, space, -,.()'" are allowed
    private HashSet<Reason> reasonList = new HashSet<>(); // must not be empty when set (must have at least one reson to visit)

    enum Reason {
        CHECKUP, REFERRAL, TESTS, FOLLOWUP, UNWELL
    }
    //public String toDataString() {}

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) throws DataInvalidException {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        Date d = new Date();
        if (date.before(d)|| date.equals(d)){
            
         throw new DataInvalidException("durationMinutes must be 20,40,60");
        }
        this.date = date;
    }

    public int getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(int durationMinutes) throws DataInvalidException {
        if (durationMinutes != 20 && durationMinutes != 40 && durationMinutes != 60) {
            throw new DataInvalidException("durationMinutes must be 20,40,60");
        }
        this.durationMinutes = durationMinutes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws DataInvalidException {
        if (name.matches("[a-zA-Z0-9()'\".]")) {
            throw new DataInvalidException("durationMinutes must be   20,40,60");
        }
        this.name = name;
    }

    public HashSet<Reason> getReasonList() {
        return reasonList;
    }

    public void setReasonList(HashSet<Reason> reasonList) {
        this.reasonList = reasonList;
    }

    private static final SimpleDateFormat dateFormatFile;

    static { // static initializer
        dateFormatFile = new SimpleDateFormat("yyyy-MM-dd");
        dateFormatFile.setLenient(false);
    }

    @Override
    public String toString() {
        //return "Appointment{" + "date=" + date + ", durationMinutes=" + durationMinutes + ", name=" + name + ", reasonList=" + reasonList + '}';
        return String.format("Appointment on %s at %d for %s for %s, reasons: %s", date, durationMinutes, name, Reason.CHECKUP);
    }

    public String toDataString() {
        return String.format("Appointment on %s at %d for %s for %s, reasons: %s", date, date, durationMinutes, name, Reason.CHECKUP);

    }

}
