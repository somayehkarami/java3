/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package day07tictactoe;

import javax.swing.JButton;
import javax.swing.JOptionPane;

/**
 *
 * @author Somay
 */
public class Day07TicTacToe extends javax.swing.JFrame {
    enum Player{None,x,o};
Player[][] board = new Player[3][3];
int moveNumber =1;
    /**
     * Creates new form Day07TicTacToe
     */
    public Day07TicTacToe() {
        initComponents();
        resetGame();
    }
private void resetGame(){
    bt00.setText("");
    bt01.setText("");
    bt02.setText("");
    bt10.setText("");
    bt11.setText("");
    bt12.setText("");
    bt20.setText("");
    bt21.setText("");
    bt22.setText("");
    lblCurrPlayer.setText("X");
    lblCurrTurn.setText("1");
    for (int row = 0; row < 3; row++) {
            for (int col = 0; col < 3; col++) {
                board[row][col] = Player.None;
            }
        }
    
}
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblCurrPlayer = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblCurrTurn = new javax.swing.JLabel();
        btResetGame = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        bt00 = new javax.swing.JButton();
        bt01 = new javax.swing.JButton();
        bt02 = new javax.swing.JButton();
        bt10 = new javax.swing.JButton();
        bt11 = new javax.swing.JButton();
        bt12 = new javax.swing.JButton();
        bt20 = new javax.swing.JButton();
        bt21 = new javax.swing.JButton();
        bt22 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        lblCurrPlayer.setText("x/o");

        jLabel2.setText("Turn of player");

        jLabel3.setText("Turn number");

        lblCurrTurn.setText("1");

        btResetGame.setText("Reset game");
        btResetGame.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btResetGameActionPerformed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setLayout(new java.awt.GridLayout(3, 3, 10, 10));

        bt00.setText(" ");
        bt00.setActionCommand("0,0");
        bt00.setName("0,0"); // NOI18N
        bt00.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btClicked(evt);
            }
        });
        jPanel1.add(bt00);

        bt01.setText(" ");
        bt01.setActionCommand("0,1");
        bt01.setName("0,1"); // NOI18N
        bt01.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btClicked(evt);
            }
        });
        jPanel1.add(bt01);

        bt02.setText(" ");
        bt02.setActionCommand("0,2");
        bt02.setMaximumSize(new java.awt.Dimension(73, 73));
        bt02.setMinimumSize(new java.awt.Dimension(73, 73));
        bt02.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btClicked(evt);
            }
        });
        jPanel1.add(bt02);

        bt10.setText(" ");
        bt10.setActionCommand("1,0");
        bt10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btClicked(evt);
            }
        });
        jPanel1.add(bt10);

        bt11.setText(" ");
        bt11.setActionCommand("1,1");
        bt11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btClicked(evt);
            }
        });
        jPanel1.add(bt11);

        bt12.setText(" ");
        bt12.setActionCommand("1,2");
        bt12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btClicked(evt);
            }
        });
        jPanel1.add(bt12);

        bt20.setText(" ");
        bt20.setActionCommand("2,0");
        bt20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btClicked(evt);
            }
        });
        jPanel1.add(bt20);

        bt21.setText(" ");
        bt21.setActionCommand("2,1");
        bt21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btClicked(evt);
            }
        });
        jPanel1.add(bt21);

        bt22.setText(" ");
        bt22.setActionCommand("2,2");
        bt22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btClicked(evt);
            }
        });
        jPanel1.add(bt22);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblCurrPlayer, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(61, 61, 61)
                        .addComponent(lblCurrTurn, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 78, Short.MAX_VALUE)
                        .addComponent(btResetGame, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCurrPlayer)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(lblCurrTurn)
                    .addComponent(btResetGame))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 243, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btClicked(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btClicked
         JButton btn = (JButton)evt.getSource();
        String action = btn.getActionCommand();
        String []data = action.split(",");
        int row = Integer.parseInt(data[0]);
        int col = Integer.parseInt(data[1]);
          System.out.printf("Button clicked: %d,%d\n", row, col);
          if (board[row][col] != Player.None) { // already clicked before, not empty
            return;
        }
        String playerStr = (moveNumber % 2 == 1) ? "X" : "O";
        Player player =  (moveNumber % 2 == 1) ? Player.x : Player.o;
        btn.setText(playerStr);
        board[row][col] = player;
        // end-game conditions: current player has 3 or 9 moves done
        if (isPlayerWinner(player)) { 
            // TODO
            
            return;
        }
        
        if (moveNumber == 9) {
            // TODO: draw
        JOptionPane.showMessageDialog(this,
                String.format("No winner"),
                    " Game over",
                    JOptionPane.INFORMATION_MESSAGE);
          resetGame();
          
            return;
        }
        
        // update turn number and player
        moveNumber++;
        lblCurrTurn.setText(moveNumber + "");
        String nextPlayerStr = (moveNumber % 2 == 1) ? "X" : "O";
        lblCurrPlayer.setText(nextPlayerStr);
                              
        
        
    }//GEN-LAST:event_btClicked
private boolean isPlayerWinner(Player player) {
        // look at the board:
        // 3 in row
        //for (int i = 0; i < 3; i++) {
       // if(board[i][0]== player&& board[i][1]== board [i][1].equals(board[0][2])&& !board[0][0].equals("")){
          //   win = true;
    //}
        
             
      //  }
           
        // 3 in col
        
        // cross (two cases)
        
        // no, then the game continues
        
        
        return false;
    }
    private void btResetGameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btResetGameActionPerformed
         resetGame();
    }//GEN-LAST:event_btResetGameActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Day07TicTacToe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Day07TicTacToe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Day07TicTacToe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Day07TicTacToe.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Day07TicTacToe().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bt00;
    private javax.swing.JButton bt01;
    private javax.swing.JButton bt02;
    private javax.swing.JButton bt10;
    private javax.swing.JButton bt11;
    private javax.swing.JButton bt12;
    private javax.swing.JButton bt20;
    private javax.swing.JButton bt21;
    private javax.swing.JButton bt22;
    private javax.swing.JButton btResetGame;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblCurrPlayer;
    private javax.swing.JLabel lblCurrTurn;
    // End of variables declaration//GEN-END:variables
}
