package day10library;

public class Book {

    public Book(int id, String isbn, String titleAndAuthor, byte[] image) {
        this.id = id;
        this.isbn = isbn;
        this.titleAndAuthor = titleAndAuthor;
        this.image = image;
    }
    

    int id;
    String isbn; // 13 or 10, with hyphens, use varchar(20), require numbers and hyphens only, last character may be X (uppercase)
    String titleAndAuthor; // 2-200 characters, any
    byte[] image; // BLOB, may be null

    @Override
    public String toString() {
        return String.format("%d: %s %s",id,isbn,titleAndAuthor);
    }
    
    

}
