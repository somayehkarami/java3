package day10library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class Database {

    private static final String dbURL = "jdbc:mysql://localhost:3306/day10library";//(day08people)name of schema
    private static final String username = "root";
    private static final String password = "Elsasorin";
    private Connection conn;// no access to database outside of this class

    // open database connection
    public Database() throws SQLException {
        conn = DriverManager.getConnection(dbURL, username, password);
    }
// select 

    public ArrayList<Book> getAllBooks() throws SQLException {//data Layer
        ArrayList<Book> list = new ArrayList<>();

        String sql = "SELECT id,isbn,titleAndAuthor FROM library";//  I will not be fetching the image
        PreparedStatement statement = conn.prepareStatement(sql);
        // it is a good practice to use try-with-resources for ResultSet so it is freed up as soon as possible
        try (ResultSet result = statement.executeQuery(sql)) {
            while (result.next()) { // has next row to read
                int id = result.getInt("id");
                String isbn = result.getString("isbn");
                String titleAndAuthor = result.getString("titleAndAuthor");
                // we dont fetch the image here on purpose

                list.add(new Book(id, isbn, titleAndAuthor, null));

            }
        }
        return list;
    }
//for fetch he complete record
    Book getBookById(int id) throws SQLException {
        PreparedStatement StmtSelect = conn.prepareStatement("SELECT * FROM library WHERE id=?");
        StmtSelect.setInt(1, id);
        try (ResultSet resultset = StmtSelect.executeQuery()) {
            if (resultset.next()) {
                String isbn = resultset.getString("isbn");
                String titleAndAuthor = resultset.getString("titleAndAuthor");

                byte[] image = resultset.getBytes("image");
                return new Book(id, isbn, titleAndAuthor, image);
            } else {

                throw new SQLException("Record notfound");
            }

        }
    }
//insert

    public void addBook(Book book) throws SQLException {
        String sql = "INSERT INTO library  VALUES (NULL, ?, ?,?)";
        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setString(1, book.isbn);//for name
        statement.setString(2, book.titleAndAuthor);//for name
        statement.setBytes(3, book.image);
        statement.executeUpdate(); // for insert, update, delete
        System.out.println("Record inserted");
    }

}
